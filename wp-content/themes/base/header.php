<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel=icon href="<?php echo IMAGES_URL ?>/favicon.png" sizes="any" type="image/png">
	<?php wp_head(); ?> 
	<?php echo get_option('header_script'); ?>
</head>

<body <?php body_class(); ?>>
<?php if($facebookAppId = get_yoast_social('fbadminapp')): ?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.3&appId=<?php echo $facebookAppId; ?>&autoLogAppEvents=1"></script>
<?php endif; ?>
<header>
	<div class="container p-relative">
		<div class="row">
			<div class="col-9 col-md-12 text-md-center logo-section mb-4">
				<a class="logo" href="<?php echo BLOG_URL; ?>">
					<?php echo bloginfo('blog_name') ?>
				</a>
			</div>					
			<div class="col-md-12 d-none d-md-block pl-0 text-right mb-4">
				<?php 
				$my_menu = wp_get_nav_menu_object( 'menu-1' );
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'nav d-inline-block h-100 w-100',
						'container_id'    => 'primary-menu',
						'menu_class'      => '',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu'
					)
				); ?>
			</div>
			<div class="col-3 d-md-none text-right">
				<?php if($my_menu->count > 0): ?>
					<a class="icon-menu-mobile"  href="javascript:void(0);">
						<span class="icon-menu-mobile__line"></span>
					</a>
				<?php endif; ?>
			</div>			
			<div class="col-12 d-md-none">
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'nav d-inline-block h-100 w-100',
						'container_id'    => 'primary-menu',
						'menu_class'      => '',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu--mobile'
					)
				); ?>
			</div>				
		</div>
	</div>
</header>