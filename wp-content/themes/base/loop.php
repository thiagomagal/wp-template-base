<?php 
$c = 1; 
if(have_posts()):
	while ( have_posts() ) : the_post(); 
		$odd = (($c % 2) == 0) ? false : true;  
		$side = ($odd) ? 'l' : 'r';
		$categories = get_the_category(get_the_ID());
		$image = get_the_post_thumbnail_url(get_the_ID(), 'large');
?>
<section data-wow-delay="0.5s" class="py-5 post-item">
	<div class="container">
		<div class="row <?php echo (!$odd) ? 'flex-column-reverse flex-sm-row' : '' ; ?>">
			<?php if($odd): ?>
				<div class="col-sm-6 align-self-center wow <?php echo ($odd)? 'fadeInLeft' : 'fadeInRight'; ?> mb-4 mb-sm-0">
					<a class="post-item__image" href="<?php the_permalink(); ?>">
						<?php if($image): ?>
							<img src="<?php echo $image; ?>" class="img-fluid" />
						<?php else: ?>
							<img src="http://lorempixel.com/600/400/abstract/" class="img-fluid" />
						<?php endif; ?>
					</a>
				</div>	
			<?php endif; ?>
			<div class="col-sm-6 align-self-center wow <?php echo ($odd)? 'fadeInRight' : 'fadeInLeft'; ?> p<?php echo $side; ?>-md-5">
				<h2><a class="color-black" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<div class="my-3">
					<?php echo get_list_taxonomy(get_the_ID(), 'category', true, ', ', 'font-size-08 underline italic color-black') ?>
				</div>
				<a class="color-black" href="<?php the_permalink(); ?>">
					<?php the_excerpt(); ?>
				</a>
				<div class="mb-3 text-right">
					<a href="<?php the_permalink(); ?>" class="btn btn--big l-spacing-1 text-uppercase bold">Leia mais</a>
				</div>									
			</div>	
			<?php if(!$odd): ?>
				<div class="col-sm-6 wow align-self-center <?php echo ($odd)? 'fadeInLeft' : 'fadeInRight'; ?> mb-4 mb-sm-0">
					<a class="post-item__image" href="<?php the_permalink(); ?>">
						<?php if($image): ?>
							<img src="<?php echo $image; ?>" class="img-fluid" />
						<?php else: ?>
							<img src="http://lorempixel.com/600/400/abstract/" class="img-fluid" />
						<?php endif; ?>
					</a>
				</div>	
			<?php endif; ?>							
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="m-auto pagination">
				<?php the_pagination(); ?>
			</div>
		</div>
	</div>
</section>

<?php $c++; endwhile; ?>

<?php else: ?>

	<h2 class="py-10 text-center">Nenhum resultado encontrado</h2>

<?php endif; ?>