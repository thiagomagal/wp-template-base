<?php

if ( ! function_exists( 'assets' ) ) {
	function assets() {
		$the_theme = wp_get_theme();
		wp_enqueue_style( 'styles', get_stylesheet_directory_uri() . '/assets/dist/main.min.css', array(), $the_theme->get( 'Version' ), false );
		wp_enqueue_script( 'plugins', get_template_directory_uri() . '/assets/dist/js/plugins.js', array(), true);
		wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/dist/js/main.min.js', array(), true);
	}
}

add_action( 'wp_enqueue_scripts', 'assets' );

function setup_base(){
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'base' ),
	) );	
	show_admin_bar(false);
	define('BLOG_URL', get_bloginfo("url"));
	define('TEMPLATE_URL', get_bloginfo("template_directory"));
	define('IMAGES_URL', get_bloginfo("template_directory")."/assets/dist/img/");
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	remove_image_size('medium_large');
}
add_action( 'after_setup_theme', 'setup_base' );

/*===================================================================================
* EXCERPT
* =================================================================================*/
function wpdocs_custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
function custom_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

function get_excerpt($post_id, $num) {
        $limit = $num+1;
        $excerpt = explode(' ', get_the_excerpt($post_id), $limit);
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt)."...";
        return $excerpt;
}

function remove_head_scripts() {
   remove_action('wp_head', 'wp_print_scripts');
   remove_action('wp_head', 'wp_print_head_scripts', 9);
   remove_action('wp_head', 'wp_enqueue_scripts', 1);
 
   add_action('wp_footer', 'wp_print_scripts', 5);
   add_action('wp_footer', 'wp_enqueue_scripts', 5);
   add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

function wp_disable_emojis() {
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	//add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
	add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
}
add_action('init', 'wp_disable_emojis');



/*===================================================================================
* PAGINATION
* =================================================================================*/
function the_pagination(){
	global $wp_query;
	$big = 999999999; // need an unlikely integer
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'prev_text' => '<',
		'next_text' => '>',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages
	));
}

/*===================================================================================
* GET YOAST SOCIAL VALUES
* =================================================================================*/
function get_yoast_social($field){
	$options = get_option( 'wpseo_social' );
	return $options[$field];
}


/*===================================================================================
* DEBUG VARIABLES
* =================================================================================*/
function debug($content){
	echo '<pre class="pl-menu">';
		var_dump($content);
	echo '</pre>';
}
/*===================================================================================
* LIST TAXONOMIES
* =================================================================================*/
function get_list_taxonomy($post_id, $taxonomy, $show_url = true, $separator = ", ", $style = ""){
	$terms = wp_get_post_terms($post_id , $taxonomy);
	$count = count($terms);
	$c = 1;
	$str = "";
	if($terms):
		foreach($terms as $term):
			$url = ($show_url) ? get_term_link($term->term_id) : 'javascript:void(0);';
			$str .= "<a class='$style' data-slug='$term->slug' data-taxonomy='$taxonomy' href='$url'>$term->name</a>";
			if ($separator && $count && $c < $count ) 
				$str .= $separator;
			$c++;
		endforeach;
	endif;
	return $str;
}
/*===================================================================================
*  INCLUDES
* =================================================================================*/
include "functions/global-config.php";