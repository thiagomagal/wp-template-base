<?php get_header(); ?>

<?php $s = $_GET['s']; ?>
<section>
	<div class="container pt-5">
		<div class="row">
			<div class="col">
				<h3>Resultados para '<?php echo $s  ?>':</h3>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('loop', 'loop'); ?>

<?php get_footer(); ?>