<?php get_header(); ?>

<?php 

if(have_posts()): the_post();
	$categories = get_the_category(get_the_ID());
	$image = get_the_post_thumbnail_url(get_the_ID(), 'large');
?>

<section data-wow-delay="0.5s" class="pb-5 post-single">
	<div class="container">
		<div class="row">
			<div class="col-12 mb-5 wow fadeInUp mb-5 post-single__highlight">
				<?php if($image): ?>
					<img class="img-fluid w-100" src="<?php echo $image; ?>" class="img-fluid" />
				<?php else: ?>
					<img class="img-fluid w-100" src="http://lorempixel.com/1200/400/abstract/" class="img-fluid" />
				<?php endif; ?>
			</div>	
			<div class="col-sm-10 offset-md-1 align-self-center wow">
				<h1><?php the_title(); ?></h1>
				<p class="italic"><?php the_date(); ?></p>
				<?php the_content(); ?>
				<div class="mb-3">
					<?php echo get_list_taxonomy(get_the_ID(), 'category', true, ', ', 'font-size-08 underline italic color-black') ?>
				</div>									
			</div>							
		</div>
	</div>
</section>

<section>
	<div class="container py-5">
		<div class="row">
			<div class="col-12 text-center">
				<h3 class="d-inline-block bold text-uppercase font-w-700 mb-4">Compartilhar</h3> <br>
				<?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox_'.get_option('addthis_id').'"]') ?>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container py-5">
		<div class="row">
			<div class="col-12 text-center mb-4">
				<h3 class="d-inline-block bold text-uppercase font-w-700">Deixe seu comentário</h3>
			</div>
			<div class="col-md-8 offset-md-2 text-center">
				<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-numposts="5"></div>		
			</div>			
		</div>
	</div>
</section>

<?php endif; ?>

<?php get_footer(); ?>