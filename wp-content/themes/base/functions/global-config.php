<?php
/*===================================================================================
* ADD GLOBAL SITE CONFIG
* =================================================================================*/
function theme_settings_page(){ 
	?>
	<div class="wrap">
		<form method="post" action="options.php">
			<?php
			settings_fields("section");
			do_settings_sections("theme-options");
			submit_button();
			?>
		</form>
	</div>
	<?php 
}

function display_addthis_id(){ ?>
	<input type="text" name="addthis_id" placeholder="Addthis ID" value="<?php echo get_option('addthis_id'); ?>" size="60">
<?php }

function display_header_script(){ ?>
	<textarea cols="61" rows="15"  name="header_script" placeholder="Header Scripts" ><?php echo get_option('header_script'); ?></textarea>
<?php }



function display_custom_info_fields(){

	add_settings_section("section", "Configurações Site", null, "theme-options");
	
	add_settings_field("addthis_id", "Addthis ID", "display_addthis_id", "theme-options", "section");
	add_settings_field("header_script", "Header Scripts", "display_header_script", "theme-options", "section");
	
	register_setting("section", "addthis_id");
	register_setting("section", "header_script");

}

add_action("admin_init", "display_custom_info_fields");

function add_custom_info_menu_item(){	
	add_options_page("Configurações Site", "Configurações Site", "manage_options", "contact-info", "theme_settings_page");
}

add_action("admin_menu", "add_custom_info_menu_item");