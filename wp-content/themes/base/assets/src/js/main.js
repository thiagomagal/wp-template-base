$ = jQuery;

$("document").ready(function(){
	 
	$(".gallery[rel=gallery]").fancybox({
		ajax:true,
		width  : '800px'
	});

	$('.highlights').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:false, 
	    autoplay: true,
	    autoplayTimeout: 7000,	    
	    items:1 ,
	    dots: true
	});

	var top = 100;
  	$(window).scroll(function(event){
  	   var current = $(this).scrollTop();		
      if (current > top){
         $('header').addClass('reduced');
      }
      else {
         $('header').removeClass('reduced');
      }
  	});
 	
  	$(".icon-menu-mobile").click(function(){
  		$(this).toggleClass("close");
  		$("header").toggleClass("open"); 
  	});
 
	var wow = new WOW(
	  {
	    boxClass:     'wow',      // animated element css class (default is wow)
	    animateClass: 'animated', // animation css class (default is animated)
	    offset:       0,          // distance to the element when triggering the animation (default is 0)
	    mobile:       true,       // trigger animations on mobile devices (default is true)
	    live:         true,       // act on asynchronously loaded content (default is true)
	    scrollContainer: null // optional scroll container selector, otherwise use window
	  }
	);
	wow.init(); 
      
  	$(".scroll-to").click(function(){
  		var element = $(this).data("element");
  		scrollTo(element);
  	});	
    
	document.addEventListener( 'wpcf7mailsent', function( event ) {
		if(event.detail.contactFormId == 99){
		 	fbq('track', 'Lead'); 	
	  	}
	}, false );
 
});


function is_mobile(){
	if($(window).width() <= 768)
		return true;
	else
		return false;
}

function scrollTo(element){
	$('html, body').animate({ scrollTop: $(element).offset().top }, 1000); 
}
