<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'temlate-base');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|_JRi>,GZfBd{/N=STq+!]u8V3/$[!HtT{i2WNqwBE&^%mhAt$NSDmZ+FOQmlq|~');
define('SECURE_AUTH_KEY',  '!N4%.G3cu[rZB@;2`+i3s!o$.0&uRMoBRX0#(>tx4/~^~] gUU[3IleM8AOS6?92');
define('LOGGED_IN_KEY',    'v z_@/c&-?HA[3,T*wQbF^OSPxQu_5&UAo&3J!;6BWXb(Md6i$r%e<EhAy}_Di/j');
define('NONCE_KEY',        'ntaJ#>>Of-[_YrP?UvMWf|yM3$5y]|;0HT`Be;](?AJumNG#DizXHWkmF!= $*@,');
define('AUTH_SALT',        '*JjTA5Y/9fnM$5ve_`kM]_Hl4>2YzgWx]0jG!,on}en[Xhnb+-BJm`K:W<,c7vfj');
define('SECURE_AUTH_SALT', '~L+].2%<NXooXg{wDHC{kxkrN%.j}_-H`npQ],&-.=X+)j%5Pc7ws.$<4fK >-oY');
define('LOGGED_IN_SALT',   '^Xuo@{nxo^#|^nKg^YZpTNo{rObHg7TzX?<g$ob??=g#qX{9r~D9Kr/MBfBJ(6{{');
define('NONCE_SALT',       'G,//g>]b<Q=p2%T3mSocEO)q|uS#p;QHFV2%,1C]OnwP7@6/!B}aY|.x7Vmej7&F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'base_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-temlate-base/wp');
define('WP_HOME', 'http://' . $_SERVER['SERVER_NAME'] .'/wp-temlate-base' );
define('WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp-temlate-base/wp-content' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');